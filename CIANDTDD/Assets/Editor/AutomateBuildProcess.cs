﻿using UnityEditor;
using UnityEngine;
using UnityEditor.Callbacks;

public class AutomateBuildProcess : MonoBehaviour
{
    public static void StartBuild()
    {
        Debug.Log("Started to build :)");

        CreateAndroidBuild();
    }

    private static void CreateAndroidBuild()
    {
        //EditorUserBuildSettings.SwitchActiveBuildTarget(BuildTarget.Android);
        PlayerSettings.applicationIdentifier = "com.matmi.autoTest";

        BuildPlayerOptions buildPlayerOptions = new BuildPlayerOptions();
        buildPlayerOptions.scenes = new[] { "Assets/Scenes/SampleScene.unity" };
        buildPlayerOptions.locationPathName = "Builds/Android/android.apk";
        buildPlayerOptions.target = BuildTarget.Android;
        buildPlayerOptions.options = BuildOptions.None;

        BuildPipeline.BuildPlayer(buildPlayerOptions);
    }

    [PostProcessBuildAttribute(0)]
    public static void OnPostProcessBuild(BuildTarget target, string pathToBuiltProject)
    {
        Debug.Log("Build finished :)");
    }
}
